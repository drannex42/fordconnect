import { writable } from 'svelte/store'

//----------------------------------------------------
// This is a mutable key value store, mostly. Here is where temporary information and/or high IO variables are placed. 
// Anything that should be held in localStorage/SharedPrefs/&c should be handled by @components/api/storage instead.
//-----------------------------------------------------

// This is the userBackground, a user selected background that appears in most of the app.
export var userBg = writable("url('/img/backgrounds/desert.jpg')"); 

// This is the background of the current page. When creating a new page, be sure this is called and updated to either a custom color or to the User Background! 
export var currentBg = writable("url('/img/backgrounds/desert.jpg')"); 

// This is used on login (callback), this is different than the access_token saved in storage.
export var access_code = writable("Unknown");

// The currently selected vehicle from a list
export var selectedVehicle = writable(0); 

// Vehicle Info 
export var storeVehicleInfo = writable({});

export var first_load = writable(true);
