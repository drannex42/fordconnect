import {getApplication, getAccessToken, checkAccess} from './user';
import {setStorageToken, getStorageToken} from './storage';

import ky from 'ky';


export async function getVehicleList() {
    const Application = JSON.parse(await getApplication());
    const access_token = await getAccessToken();

    const req_uri = "https://api.mps.ford.com/api/fordconnect/vehicles/v1";

    const req = ky.create({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api-version': Application.api_version,
            'Application-Id': Application.application_id,
            'Authorization': 'Bearer ' + access_token,
        }
    });

    const req_res = await req(req_uri).json();
    const StorageVehicle = await setStorageToken("Vehicle", JSON.stringify(req_res));

    const loadVehicleInfo = await getVehicleInfo();
    return req_res;
}

export async function getVehicleInfo() {
    const Application = JSON.parse(await getApplication());
    const access_token = await getAccessToken();

    const Vehicle = JSON.parse(await getStorageToken("Vehicle"));

    if (!Vehicle) {
        console.log("[Vehicle] :: No vehicle information detected in storage")
        const getVehicle = await getVehicleList();
        return { message: 'Unable to Get Vehicle'}
    }

    console.log("[Vehicle] :: Vehicle information detected in storage")

    const req_uri = "https://api.mps.ford.com/api/fordconnect/vehicles/v1/" + Vehicle.vehicles[0].vehicleId; 
   
    const req = ky.create({
        headers: {
            'api-version': Application.api_version,
            'Application-Id': Application.application_id,
            'Authorization': 'Bearer ' + access_token,
        }
    });

    const req_res = await req(req_uri).json();

    const SelectedVehicleInfo = await setStorageToken("SelectedVehicle", JSON.stringify(req_res));

    return req_res;
}

// setPowerStatus()/1
// There is no reason to create multiple functions to handle basic vehicle commands, this takes a 'command' argument and parses for the correct command type.
// existing commands: 'unlock', 'lock', 'startEngine', 'stopEngine', and 'wake'. 

export async function setPowerStatus(command) {
    const Application = JSON.parse(await getApplication());
    const access_token = await getAccessToken();
    const Vehicle = JSON.parse(await getStorageToken("Vehicle"));

    if (!Vehicle) {
        console.log("[Vehicle] :: No vehicle information detected in storage")
        const getVehicle = await getVehicleList();
        return { message: 'Unable to Get Vehicle'}
    }
    
    const req_uri = "https://api.mps.ford.com/api/fordconnect/vehicles/v1/" + Vehicle.vehicles[0].vehicleId + "/" + command;

    var setHeaders = {
        'api-version': Application.api_version,
        'Application-Id': Application.application_id,
        'Authorization': 'Bearer ' + access_token,
    };

    // While running without setting the correct headers is possible, I believe that following the spec published in the Postman collection to be absolutely vital.

    if (command == "unlock") {
        setHeaders.Accept = "*/*";
        setHeaders["Content-Type"] = 'application/json';
    }
    if (command == "lock") {
        setHeaders.Accept = "*/*";
        setHeaders["Content-Type"] = 'application/json';
    }
    if (command == "wake") {
        setHeaders.Accept = "application/json";
        setHeaders["Content-Type"] = 'application/json';
        setHeaders['callback-url'] = "none";
    }

    const req_res = await ky.post(req_uri, {headers: setHeaders}).json();
    console.log(req_res);
    if (req_res.commandStatus == "COMPLETED") {
        return {status: "200", stack: req_res}
    } else {
        return {msg: "Error: Could not complete the requested action, " + command, stack: req_res};
    }
}

// saveVehicleImage()/1
// Saves the vehicleImage asset url, must specifiy category of 'thumnbail' or 'full' to storage.

export async function saveVehicleImage(category) {
    const Application = JSON.parse(await getApplication());
    const access_token = await getAccessToken();
    const Vehicle = JSON.parse(await getStorageToken("Vehicle"));
    var vehicleMake;

    if (Vehicle.vehicles[0].make == "F") {
        vehicleMake = "Ford"
    };

    if (Vehicle.vehicles[0].make == "L") {
        vehicleMake = "Lincoln"
    };

    var req_uri = 
        'https://api.mps.ford.com/api/fordconnect/vehicles/v1/'
             + Vehicle.vehicles[0].vehicleId + '/images/' + category + 
             '?make=' + vehicleMake + 
             '&model=' + Vehicle.vehicles[0].modelName + // Appears the API does not load different models car thumbnails yet....
             '&year=' + Vehicle.vehicles[0].modelYear;

   const req = ky.create({
        headers: {
            'api-version': Application.api_version,
            'Application-Id': Application.application_id,
            'Authorization': 'Bearer ' + access_token, 
        }
   });

    const req_res = await req(req_uri).arrayBuffer();

    function bufferToBase64(buffer) {
        return btoa(new Uint8Array(buffer).reduce((data, byte)=> {
        return data + String.fromCharCode(byte);
        }, ''));
    }

    const image64 = bufferToBase64(req_res);

    setStorageToken('vehicleImage', image64)
    return {'msg': '[Vehicle] :: Vehicle Image saved to storage'};
}

export async function getVehicleImage(category) {
    saveVehicleImage(category);

    var VehicleImage = await getStorageToken('vehicleImage');
    console.log('[Vehicle: Image] ::',  VehicleImage);
    return VehicleImage;
}

export async function getVehicleStatus() {
    const Application = JSON.parse(await getApplication());
    const access_token = await getAccessToken();
    const Vehicle = await getVehicleInfo();

    async function reqCommand() {
        const command_req_uri = "https://api.mps.ford.com/api/fordconnect/vehicles/v1/" + Vehicle.vehicle.vehicleId + '/status'; 
    
        const command_req = ky.post(command_req_uri, {
            headers: {
                'api-version': Application.api_version,
                'Application-Id': Application.application_id,
                'Authorization': 'Bearer ' + access_token,
                'Accept': '*/*',
                'Content-Type': 'application/json',
                'callback-url': 'none'
            }
        }).json();

        const command_req_res = await command_req;
        console.log('COMMAND', command_req_res)
        return command_req_res;
    }

    var command_res = await reqCommand();

    async function status_res() {
        if (command_res.status === "SUCCESS") {
            console.log('COMMAND_RES', command_res)
            const req_uri = "https://api.mps.ford.com/api/fordconnect/vehicles/v1/" + Vehicle.vehicle.vehicleId + '/statusrefresh/' + command_res.commandId; 
        
            const req = ky.create({
                headers: {
                    'api-version': Application.api_version,
                    'Application-Id': Application.application_id,
                    'Authorization': 'Bearer ' + access_token,
                }
            });

            const req_res = await req(req_uri).json();
            console.log('STATUS:', req_res);

            // Retry on PENDINGRESPONSE until success;
            if (req_res.commandStatus === "PENDINGRESPONSE" && req_res.status != "SUCCESS"  ) {
                return status_res();
                // If this is continually cyrcling it's because the API has a glitch where 'null' is the only response and the PENDINGRESPONSE never finishes. 
            }

            return req_res;

        } else {
            console.log("[Vehicle] :: Unable to update vehicle status. Error Message: ", command_res)
            return {msg: "[Vehicle] :: Unable to update status", trace: command_res}
        }
    }

    let res = await status_res();

    let StatusStorage = await setStorageToken('Status', res)

    return StatusStorage;
}