// tailwind.config.ts
import { defineConfig } from 'windicss/helpers'

export default defineConfig({
    darkMode: 'class',
    safelist: 'p-3 p-4 p-5',
    theme: {
        extend: {
            colors: {
                ford: "19, 58, 124"
            }
        }
    }
})
